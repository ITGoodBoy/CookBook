package com.celestialapps.cookbook;

import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;

import com.android.vending.expansion.zipfile.APKExpansionSupport;
import com.android.vending.expansion.zipfile.ZipResourceFile;

import java.io.IOException;
import java.io.InputStream;

/**
 * Created by ghost on 04.11.17.
 */

public class ExternalFileUtils {

    private Context context;
    private ZipResourceFile zipResourceFile;

    public ExternalFileUtils(Context context) {
        this.context = context;
    }

    private ZipResourceFile getZipResourceFile() {
        try {
            if (zipResourceFile == null) {
                zipResourceFile = APKExpansionSupport.getAPKExpansionZipFile(context, 4, 0);
                return zipResourceFile;
            }
            return zipResourceFile;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private InputStream getInputStream(String fileName) {
        try {
            ZipResourceFile zipResourceFile = getZipResourceFile();
            if (zipResourceFile != null)
                return getZipResourceFile().getInputStream("CookBook/" + fileName);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }


    public Bitmap getBitmapDrawable(String fileName) {
        InputStream inputStream = getInputStream(fileName);

        BitmapFactory.Options options;

        if (inputStream == null) return null;
        try {

            return BitmapFactory.decodeStream(inputStream);
        } catch (OutOfMemoryError e) {

        }
        return null;
    }

}
