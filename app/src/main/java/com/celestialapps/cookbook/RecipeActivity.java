package com.celestialapps.cookbook;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.anjlab.android.iab.v3.BillingProcessor;
import com.anjlab.android.iab.v3.BillingProcessor.IBillingHandler;
import com.anjlab.android.iab.v3.TransactionDetails;
import com.celestialapps.cookbook.model.Recipe;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;

import java.net.InetAddress;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;


/**
 * Created by ghost on 07.10.17.
 */

public class RecipeActivity extends AppCompatActivity implements IBillingHandler{
    private Recipe recipe;

    private final String LICENSE_KEY = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAju3jd8LFNMiqYUi9/iEHYTBhO2kdGZafszVBNXOXvF5JZYso1oUVOaxD4NoBo5/eurZS/1hlsnirnmNB+6ouLUzL9xi8nPO6qtGt5iXPeiIauwsvUv6oVqBL5RTd/WcVdB3QNfemnBo2/JGbZR2Hjy3JiwnCcjzqTnMkD5UzDvg348s8qoU8nraD9IdHcEURD2ZTmia4nc+h1e9jAJEkQHL9bUxUQET5MEU9V5hodxGiMF6lel6C9P9uwIMkgIhRmNAIlINObb3JQSZpNItQOyz4Xr2Q6FsvzW6cXgq827V+x3hYYGFsENAVmItG9ZeoiZbb2aEWMh7N0Hd2Csr06wIDAQAB";
    private final String CLOSE_ADS_PRODUCT_ID = "close_ads";

    private BillingProcessor billingProcessor;
    private CircleImageView circleImageView;
    private RecyclerView recyclerView;
    private TextView textView;
    private AdView mAdView;
    private ExternalFileUtils externalFileUtils;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipe);
        billingProcessor = new BillingProcessor(this, LICENSE_KEY, this);

        String recipeName = getIntent().getStringExtra("recipeName");
        recipe = Realm.getDefaultInstance().where(Recipe.class).equalTo("name", recipeName).findFirst();

        externalFileUtils = new ExternalFileUtils(getApplicationContext());
        setupWidgets();
        setupActionBar();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = null;
        if (connectivityManager != null) {
            activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        }
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    private void setupWidgets() {
        circleImageView = (CircleImageView) findViewById(R.id.circle_image_view);
        circleImageView.setImageBitmap(getRecipeDrawable(recipe.getPathToImageFromAssets()));

        recyclerView = (RecyclerView) findViewById(R.id.recycle_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setItemViewCacheSize(50);
        recyclerView.setDrawingCacheEnabled(true);
        recyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_AUTO);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new IngredientsAdapter(this, recipe.getIngredients(), true));

        textView = (TextView) findViewById(R.id.text_view_cooking_algorithm);
        textView.setText(recipe.getCookingAlgorithm());
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) return;
        actionBar.setTitle(recipe.getName());
    }

    private Bitmap getRecipeDrawable(String fileName) {
        return externalFileUtils.getBitmapDrawable(fileName);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (!billingProcessor.handleActivityResult(requestCode, resultCode, data)) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onDestroy() {
        if (billingProcessor != null) {
            billingProcessor.release();
        }
        super.onDestroy();
    }

    @Override
    public void onProductPurchased(@NonNull String productId, @Nullable TransactionDetails details) {
        LinearLayoutCompat layoutCompat = findViewById(R.id.llc);
        RelativeLayout relativeLayout = layoutCompat.findViewById(R.id.rl_ads);

        layoutCompat.removeView(relativeLayout);
    }

    @Override
    public void onPurchaseHistoryRestored() {

    }

    @Override
    public void onBillingError(int errorCode, @Nullable Throwable error) {
        if (error != null) {
            Log.e("Billing Error", errorCode + " - " + error.getMessage());
        }
    }

    @Override
    public void onBillingInitialized() {
        mAdView = findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        if (isNetworkAvailable() && !billingProcessor.isPurchased(CLOSE_ADS_PRODUCT_ID)) {
            if (mAdView != null) {
                mAdView.loadAd(adRequest);
            }

            AppCompatImageView imageView = findViewById(R.id.close_ads);

            imageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog dialog = new AlertDialog.Builder(RecipeActivity.this)
                            .setMessage("Поддержите развитие приложения отключив рекламу.\n" +
                                    "Что мы планируем в следущем обновлении:\n" +
                                    " - Возможность создавать собственные рецепты.\n" +
                                    " - возможность публиковать свои рецепты для других пользователей(при желании).\n" +
                                    " - возможность оставить отзыв о рецепте, поставить лайк.\n" +
                                    " - гибкая сортировка по любому параметру (название рецепта, кол-во лайков, кол-во коментов и т.д.).\n" +
                                    " - рецепты для вегетерианцев и веганов будут помечены специальными символами.\n" +
                                    " - отключать некоторые данные при отображении если они вам не нужны (например не показывать ингредиенты в списке рецептов).\n" +
                                    " - вы сможете написать своё предложение по улучшению приложения или же поставить лайк на предложение других пользователей.")
                            .setNegativeButton("Отмена", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    dialog.cancel();
                                }
                            })
                            .setPositiveButton("Продолжить", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    billingProcessor.purchase(RecipeActivity.this, CLOSE_ADS_PRODUCT_ID);
                                }
                            })
                            .create();

                    dialog.show();
                }
            });
        } else {
            LinearLayoutCompat layoutCompat = findViewById(R.id.llc);
            RelativeLayout relativeLayout = layoutCompat.findViewById(R.id.rl_ads);

            layoutCompat.removeView(relativeLayout);
        }
    }
}
