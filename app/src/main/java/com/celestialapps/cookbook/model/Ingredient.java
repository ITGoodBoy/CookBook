package com.celestialapps.cookbook.model;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by ghost on 17.09.17.
 */

public class Ingredient extends RealmObject{

    @PrimaryKey
    private String name;
    private String count;
    private String pathToImageFromAssets;
    // 0 - not selected, 1 - show recipes with this ingredient, 2 - don't show recipes with this ingredient
    private int state;

    public Ingredient() {
    }



    public Ingredient(String name, String count) {
        this.name = name;
        this.count = count;
    }

    public Ingredient(String name, String count, String pathToImageFromAssets) {
        this.name = name;
        this.count = count;
        this.pathToImageFromAssets = pathToImageFromAssets;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getPathToImageFromAssets() {
        return pathToImageFromAssets;
    }

    public void setPathToImageFromAssets(String pathToImageFromAssets) {
        this.pathToImageFromAssets = pathToImageFromAssets;
    }

    public int getState() {
        return state;
    }

    public void setState(int state) {
        this.state = state;
    }
}
