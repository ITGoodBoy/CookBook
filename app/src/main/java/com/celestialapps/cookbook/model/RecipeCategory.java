package com.celestialapps.cookbook.model;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by ghost on 18.09.17.
 */

public class RecipeCategory extends RealmObject implements Serializable{

    private String name;
    private RealmList<Recipe> recipes;
    private String pathToImageFromAssets;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public RealmList<Recipe> getRecipes() {
        return recipes;
    }

    public void setRecipes(RealmList<Recipe> recipes) {
        this.recipes = recipes;
    }

    public String getPathToImageFromAssets() {
        return pathToImageFromAssets;
    }

    public void setPathToImageFromAssets(String pathToImageFromAssets) {
        this.pathToImageFromAssets = pathToImageFromAssets;
    }
}
