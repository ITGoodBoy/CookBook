package com.celestialapps.cookbook.model;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import java.io.Serializable;
import java.util.List;

import io.realm.RealmList;
import io.realm.RealmObject;

/**
 * Created by ghost on 17.09.17.
 */

public class Recipe extends RealmObject{

    private String name;
    private RealmList<Ingredient> ingredients;
    private String cookingAlgorithm;
    private String pathToImageFromAssets;
    private boolean isFavorite;
    private int checkedCount;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public void setIngredients(RealmList<Ingredient> ingredients) {
        this.ingredients = ingredients;
    }

    public String getCookingAlgorithm() {
        return cookingAlgorithm;
    }

    public void setCookingAlgorithm(String cookingAlgorithm) {
        this.cookingAlgorithm = cookingAlgorithm;
    }

    public String getPathToImageFromAssets() {
        return pathToImageFromAssets;
    }

    public void setPathToImageFromAssets(String pathToImageFromAssets) {
        this.pathToImageFromAssets = pathToImageFromAssets;
    }

    public void increaseCheckedCount() {
        checkedCount++;
    }

    public void enlargeCheckedCount() {
        checkedCount--;
    }

    public int getCheckedCount() {
        return checkedCount;
    }

    public void setCheckedCount(int checkedCount) {
        this.checkedCount = checkedCount;
    }

    public boolean isFavorite() {
        return isFavorite;
    }

    public void setFavorite(boolean favorite) {
        isFavorite = favorite;
    }
}
