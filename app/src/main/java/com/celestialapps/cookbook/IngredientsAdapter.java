package com.celestialapps.cookbook;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import com.celestialapps.cookbook.model.Ingredient;
import com.celestialapps.cookbook.model.Recipe;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;
import io.realm.Realm.Transaction;

/**
 * Created by ghost on 24.09.17.
 */

public class IngredientsAdapter extends RecyclerView.Adapter<IngredientsAdapter.IngredientViewHolder> {

    private LayoutInflater mLayoutInflater;
    private List<Ingredient> mIngredients;
    private List<Ingredient> mIngredientsCopy;
    private List<Ingredient> mIngredientsCopy2;

    private SearchActivity searchActivity;
    private Recipe recipe;
    private boolean mWithCount;
    private boolean mWithCheckBox;
    private SingleRecipesAdapter mSingleRecipesAdapter;
    private AutoCompleteTextView mAutoCompleteTextView;
    private ExternalFileUtils mExternalFileUtils;


    public IngredientsAdapter(SearchActivity searchActivity, boolean withCheckBox, Context context, List<Ingredient> ingredients, SingleRecipesAdapter singleRecipesAdapter, AutoCompleteTextView autoCompleteTextView) {
        this.searchActivity = searchActivity;
        this.mLayoutInflater = LayoutInflater.from(context);
        this.mIngredients = ingredients;
        this.mWithCheckBox = withCheckBox;
        this.mSingleRecipesAdapter = singleRecipesAdapter;
        this.mAutoCompleteTextView = autoCompleteTextView;
        this.mExternalFileUtils = new ExternalFileUtils(context);

        this.mIngredientsCopy = new ArrayList<>();
        this.mIngredientsCopy.addAll(mIngredients);

        this.mIngredientsCopy2 = new ArrayList<>();
        this.mIngredientsCopy2.addAll(mIngredients);
    }

    public IngredientsAdapter(Context context, Recipe recipe) {
        this.mLayoutInflater = LayoutInflater.from(context);
        this.mIngredients = recipe.getIngredients();
        this.mExternalFileUtils = new ExternalFileUtils(context);
        this.recipe = recipe;
    }

    public IngredientsAdapter(Context context, List<Ingredient> ingredients, boolean withCount) {
        this.mLayoutInflater = LayoutInflater.from(context);
        this.mIngredients = ingredients;
        this.mExternalFileUtils = new ExternalFileUtils(context);
        this.mWithCount = withCount;
    }

    @Override
    public IngredientViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view;
        if (mWithCheckBox) {
            view = mLayoutInflater.inflate(R.layout.ingredients_item_with_chek_box, parent, false);
        } else {
            view = mLayoutInflater.inflate(R.layout.activity_ingredients_recycler_view_items, parent, false);

            if (!mWithCount) view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    view.getContext().startActivity(new Intent(view.getContext(), RecipeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                            .putExtra("recipeName", recipe.getName()));
                }
            });
        }

        return new IngredientViewHolder(view);
    }


    @Override
    public void onBindViewHolder(IngredientViewHolder holder, int position) {
        final Ingredient ingredient;

        if (mWithCheckBox)
            ingredient = mIngredientsCopy.get(position);
        else
            ingredient = mIngredients.get(position);

        holder.appCompatImageView.setImageBitmap(getIngredientDrawable(ingredient.getPathToImageFromAssets()));

        if (mWithCount)
            holder.appCompatTextView.setText(ingredient.getName().trim() + " - " + ingredient.getCount().trim());
        else holder.appCompatTextView.setText(ingredient.getName().trim());

        if (mWithCheckBox) {
            final AppCompatImageView imageView = holder.mainView.findViewById(R.id.checkbox_ingredient);
            imageView.setImageResource(R.drawable.list_drawable);
            imageView.setOnClickListener(null);

            switch (ingredient.getState()) {
                case 0:
                    imageView.setImageLevel(0);
                    break;
                case 1:
                    imageView.setImageLevel(1);
                    break;
                case 2:
                    imageView.setImageLevel(2);
                    break;
            }

            imageView.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    Realm.getDefaultInstance().executeTransaction(new Transaction() {
                        @Override
                        public void execute(Realm realm) {

                            switch (ingredient.getState()) {
                                case 0:
                                    ingredient.setState(1);
                                    imageView.setImageLevel(1);

                                    List<Recipe> recipes = Realm.getDefaultInstance()
                                            .where(Recipe.class)
                                            .equalTo("ingredients.name", ingredient.getName())
                                            .findAll();

                                    for (Recipe recipe : recipes) {
                                        recipe.increaseCheckedCount();
                                    }

                                    break;
                                case 1:
                                    ingredient.setState(2);
                                    imageView.setImageLevel(2);

                                    break;
                                case 2:
                                    ingredient.setState(0);
                                    imageView.setImageLevel(0);

                                    recipes = Realm.getDefaultInstance()
                                            .where(Recipe.class)
                                            .equalTo("ingredients.name", ingredient.getName())
                                            .findAll();

                                    for (Recipe recipe : recipes) {
                                        recipe.enlargeCheckedCount();
                                    }

                                    break;
                            }

                            refreshSingleRecipesAdapter();
                        }
                    });

                }
            });

        }
    }


    public void refreshSingleRecipesAdapter() {
        List<Recipe> recipes = SplashActivity.getRecipeListByIngredientsChecked();
        searchActivity.setSearchRecipes(recipes);
        mSingleRecipesAdapter.setDataAndRefresh(recipes);
        mAutoCompleteTextView.setAdapter(new ArrayAdapter<>(mAutoCompleteTextView.getContext(),
                android.R.layout.simple_dropdown_item_1line,
                getRecipeNames()));
    }

    public void filter(String text) {
        mIngredientsCopy.clear();
        if (text.isEmpty()) {
            mIngredientsCopy.addAll(mIngredientsCopy2);
        } else {
            text = text.toLowerCase();
            for(Ingredient ingredient: mIngredientsCopy2){
                if(ingredient.getName().toLowerCase().contains(text)){
                    mIngredientsCopy.add(ingredient);
                }
            }
        }
        notifyDataSetChanged();
    }

    private List<String> getRecipeNames() {
        List<String> list = new ArrayList<>();

        for (Recipe recipe : searchActivity.SEARCH_RECIPES) {
            list.add(recipe.getName());
        }

        return list;
    }



    private Bitmap getIngredientDrawable(String fileName) {
        return mExternalFileUtils.getBitmapDrawable(fileName);
    }

    @Override
    public int getItemCount() {
        if (mWithCheckBox) return mIngredientsCopy.size();

        return mIngredients.size();
    }

    static class IngredientViewHolder extends RecyclerView.ViewHolder {

        private View mainView;
        private AppCompatTextView appCompatTextView;
        private CircleImageView appCompatImageView;

        public IngredientViewHolder(View itemView) {
            super(itemView);
            this.mainView = itemView;
            appCompatTextView = itemView.findViewById(R.id.ingredients_app_compat_text_view_item);
            appCompatImageView = itemView.findViewById(R.id.ingredients_app_compat_image_view_item);
        }
    }
}
