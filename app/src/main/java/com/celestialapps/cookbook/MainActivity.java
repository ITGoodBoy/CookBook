package com.celestialapps.cookbook;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.GridLayout;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import com.celestialapps.cookbook.model.RecipeCategory;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;

public class MainActivity extends AppCompatActivity {

    private GridLayout mGridLayout;
    private ExternalFileUtils mExternalFileUtils;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupActionBar();
        
        mGridLayout = (GridLayout) findViewById(R.id.main_grid_layout_v7_categories);
        mExternalFileUtils = new ExternalFileUtils(getApplicationContext());

        final List<RecipeCategory> recipeCategories = Realm.getDefaultInstance().where(RecipeCategory.class).findAll();

        for (int i = 0; i < recipeCategories.size(); i++) {
            RelativeLayout linearLayoutCompat = (RelativeLayout) mGridLayout.getChildAt(i);
            linearLayoutCompat.setId(i);

            AppCompatTextView recipeNameTextView = (AppCompatTextView) linearLayoutCompat.getChildAt(0);
            CircleImageView recipeImage = (CircleImageView) linearLayoutCompat.getChildAt(1);

            linearLayoutCompat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    String recipeCategoryName = recipeCategories.get(view.getId()).getName();
                    startActivity(new Intent(getApplicationContext(), RecipesActivity.class).putExtra("recipeCategoryName", recipeCategoryName));
                }
            });

            RecipeCategory recipeCategory = recipeCategories.get(i);

            recipeNameTextView.setText(recipeCategory.getName());

            Bitmap bitmap = mExternalFileUtils.getBitmapDrawable(recipeCategory.getPathToImageFromAssets());
            recipeImage.setImageBitmap(bitmap);

        }

    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) return;
        actionBar.setTitle("Кулинарная книга");
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.search_by_ingredient) {
            startActivity(new Intent(getApplicationContext(), SearchActivity.class));
        }

        if (item.getItemId() == R.id.favourite_recipes) {
            startActivity(new Intent(getApplicationContext(), RecipesActivity.class).putExtra("isFavorite", true));
        }
        return super.onOptionsItemSelected(item);
    }
}
