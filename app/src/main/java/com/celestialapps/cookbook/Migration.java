package com.celestialapps.cookbook;

import io.realm.DynamicRealm;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmSchema;

/**
 * Created by Sergey on 22.06.2018.
 */

public class Migration implements RealmMigration {

    // Migration logic...

    @Override
    public int hashCode() {
        return 37;
    }

    public boolean equals(Object object) {
        return object != null && object instanceof Migration;
    }

    @Override
    public void migrate(DynamicRealm realm, long oldVersion, long newVersion) {
        if (oldVersion == 0) {
            RealmSchema schema = realm.getSchema();
            RealmObjectSchema objectSchema = schema.get("Ingredient");

            if (objectSchema != null) {
                objectSchema.removeField("isChecked");
                objectSchema.addField("state", int.class);
            }

            oldVersion++;
        }
    }
}
