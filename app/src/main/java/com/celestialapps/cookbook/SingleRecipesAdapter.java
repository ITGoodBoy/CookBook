package com.celestialapps.cookbook;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.celestialapps.cookbook.model.Recipe;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ghost on 14.10.17.
 */

public class SingleRecipesAdapter extends RecyclerView.Adapter<SingleRecipesAdapter.SingleRecipeHolder> {

    private Context mContext;
    private LayoutInflater mInflater;
    private List<Recipe> mRecipes;

    private List<Recipe> mRecipesCopy;
    private List<Recipe> mRecipesCopy2;
    private ExternalFileUtils mExternalFileUtils;

    public SingleRecipesAdapter(Context context, List<Recipe> recipes) {
        this.mContext = context;
        this.mRecipes = recipes;
        this.mInflater = LayoutInflater.from(mContext);
        this.mExternalFileUtils = new ExternalFileUtils(context);

        mRecipesCopy = new ArrayList<>();
        mRecipesCopy.addAll(mRecipes);

        mRecipesCopy2 = new ArrayList<>();
        mRecipesCopy2.addAll(mRecipes);
    }

    public void setDataAndRefresh(List<Recipe> recipes) {
        this.mRecipesCopy = new ArrayList<>();
        this.mRecipesCopy.addAll(recipes);

        this.mRecipesCopy2 = new ArrayList<>();
        this.mRecipesCopy2.addAll(recipes);

        notifyDataSetChanged();
    }


    @Override
    public SingleRecipeHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.single_recipe_item, parent, false);
        return new SingleRecipeHolder(view);
    }

    @Override
    public void onBindViewHolder(SingleRecipeHolder holder, int position) {
        final Recipe recipe = mRecipesCopy.get(position);

        holder.appCompatTextView.setText(recipe.getName());
        holder.appCompatImageView.setImageBitmap(getRecipeDrawable(recipe.getPathToImageFromAssets()));


        final RelativeLayout relativeLayout = holder.relativeLayout;
        final Context context = relativeLayout.getContext();

        relativeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, RecipeActivity.class).addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
                        .putExtra("recipeName", recipe.getName()));
            }
        });
    }


    private Bitmap getRecipeDrawable(String fileName) {
        return mExternalFileUtils.getBitmapDrawable(fileName);
    }


    public void filter(String text) {
        mRecipesCopy.clear();
        if(text.isEmpty()){
            mRecipesCopy.addAll(mRecipesCopy2);
        } else {
            text = text.toLowerCase();
            for(Recipe recipe : mRecipesCopy2){
                if(recipe.getName().toLowerCase().contains(text)){
                    mRecipesCopy.add(recipe);
                }
            }
        }
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mRecipesCopy.size();
    }

    static class SingleRecipeHolder extends RecyclerView.ViewHolder {

        private RelativeLayout relativeLayout;
        private AppCompatTextView appCompatTextView;
        private CircleImageView appCompatImageView;

        public SingleRecipeHolder(View itemView) {
            super(itemView);
            relativeLayout = itemView.findViewById(R.id.main_relative2);
            appCompatTextView = itemView.findViewById(R.id.recipes_app_compat_text_view_item2);
            appCompatImageView = itemView.findViewById(R.id.recipes_app_compat_image_view_item2);
        }
    }
}
