package com.celestialapps.cookbook;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.celestialapps.cookbook.model.Recipe;
import com.celestialapps.cookbook.model.RecipeCategory;

import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by ghost on 24.09.17.
 */

public class RecipesActivity extends AppCompatActivity {

    private RecyclerView mRecyclerViewRecipes;
    private boolean isFavorite;
    private String recipeCategoryName;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipes);

        recipeCategoryName = getIntent().getStringExtra("recipeCategoryName");
        isFavorite = getIntent().getBooleanExtra("isFavorite", false);

        setupActionBar();

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);

        mRecyclerViewRecipes = findViewById(R.id.recipes_recycler_view_recipes);
        mRecyclerViewRecipes.setItemViewCacheSize(50);
        mRecyclerViewRecipes.setDrawingCacheEnabled(true);
        mRecyclerViewRecipes.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_AUTO);
        mRecyclerViewRecipes.setLayoutManager(linearLayoutManager);
        mRecyclerViewRecipes.addItemDecoration(new DividerItemDecoration(this, linearLayoutManager.getOrientation()));

        if (isFavorite) {
            RealmResults<Recipe> favouriteRecipes = Realm.getDefaultInstance()
                    .where(Recipe.class)
                    .equalTo("isFavorite", true).findAllSorted("name", Sort.ASCENDING);

            mRecyclerViewRecipes.setAdapter(new RecipesAdapter(this, favouriteRecipes));
        } else {
            RecipeCategory recipeCategory = Realm.getDefaultInstance()
                    .where(RecipeCategory.class).equalTo("name", recipeCategoryName).findFirst();

            mRecyclerViewRecipes.setHasFixedSize(true);
            mRecyclerViewRecipes.setAdapter(new RecipesAdapter(this, recipeCategory.getRecipes()));
        }
    }

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar == null) return;

        if (isFavorite) {
            actionBar.setTitle("Закладки");
        } else {
            actionBar.setTitle(recipeCategoryName);
        }
    }
}
