package com.celestialapps.cookbook;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.LinearLayoutCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RelativeLayout;

import com.celestialapps.cookbook.RecipesAdapter.RecipesViewHolder;
import com.celestialapps.cookbook.model.Ingredient;
import com.celestialapps.cookbook.model.Recipe;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.OrderedRealmCollection;
import io.realm.Realm;
import io.realm.Realm.Transaction;
import io.realm.RealmList;
import io.realm.RealmRecyclerViewAdapter;
import io.realm.RealmResults;

/**
 * Created by ghost on 24.09.17.
 */

public class RecipesAdapter extends RealmRecyclerViewAdapter<Recipe, RecipesViewHolder> {

    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private OrderedRealmCollection<Recipe> mRecipes;
    private ExternalFileUtils mExternalFileUtils;


    public RecipesAdapter(Context context, RealmResults<Recipe> recipes) {
        super(recipes, true, true);
        this.mContext = context;
        this.mLayoutInflater = LayoutInflater.from(context);
        this.mRecipes = recipes;
        this.mExternalFileUtils = new ExternalFileUtils(context);
    }

    public RecipesAdapter(Context context, RealmList<Recipe> recipes) {
        super(recipes, false, false);
        this.mContext = context;
        this.mLayoutInflater = LayoutInflater.from(context);
        this.mRecipes = recipes;
        this.mExternalFileUtils = new ExternalFileUtils(context);
    }



    @Override
    public RecipesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mLayoutInflater.inflate(R.layout.activity_recipes_recycler_view_items, parent, false);

        return new RecipesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecipesViewHolder holder, int position) {
        final Recipe recipe = mRecipes.get(position);

        holder.appCompatTextView.setText(recipe.getName());
        holder.appCompatImageView.setImageBitmap(getRecipeDrawable(recipe.getPathToImageFromAssets()));

        if (recipe.isFavorite()) {
            holder.favouriteImageView.setChecked(true);
        } else {
            holder.favouriteImageView.setChecked(false);
        }

        holder.favouriteImageView.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, final boolean isChecked) {
                Realm.getDefaultInstance().executeTransaction(new Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        recipe.setFavorite(isChecked);
                    }
                });
            }
        });

        holder.linearLayoutCompat.removeAllViews();

     //   if (holder.linearLayoutCompat.getChildCount() == 0) {
            for (Ingredient ingredient : recipe.getIngredients()) {
                View view = mLayoutInflater.inflate(R.layout.activity_ingredients_recycler_view_items, holder.linearLayoutCompat, false);
                AppCompatTextView appCompatTextView = view.findViewById(R.id.ingredients_app_compat_text_view_item);
                CircleImageView circleImageView = view.findViewById(R.id.ingredients_app_compat_image_view_item);

                appCompatTextView.setText(ingredient.getName().trim());
                circleImageView.setImageBitmap(getIngredientDrawable(ingredient.getPathToImageFromAssets()));

                holder.linearLayoutCompat.addView(view);
            }
      //  }


        final ConstraintLayout constraintLayout = holder.constraintLayout;
        final Context context = constraintLayout.getContext();

        constraintLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                context.startActivity(new Intent(context, RecipeActivity.class)
                        .addFlags(Intent.FLAG_ACTIVITY_NEW_TASK).putExtra("recipeName", recipe.getName()));
            }
        });
    }

    private Bitmap getIngredientDrawable(String fileName) {
        return mExternalFileUtils.getBitmapDrawable(fileName);
    }

    private Bitmap getRecipeDrawable(String fileName) {
        return mExternalFileUtils.getBitmapDrawable(fileName);
    }

    @Override
    public int getItemCount() {
        return mRecipes.size();
    }

    static class RecipesViewHolder extends RecyclerView.ViewHolder  {

        private ConstraintLayout constraintLayout;
        private AppCompatTextView appCompatTextView;
        private CircleImageView appCompatImageView;
        private LinearLayoutCompat linearLayoutCompat;
        private AppCompatCheckBox favouriteImageView;

        public RecipesViewHolder(final View itemView) {
            super(itemView);
            constraintLayout = itemView.findViewById(R.id.main_relative);
            appCompatTextView = itemView.findViewById(R.id.recipes_app_compat_text_view_item);
            appCompatImageView = itemView.findViewById(R.id.recipes_app_compat_image_view_item);

            linearLayoutCompat = itemView.findViewById(R.id.linear_layout_compat);
            favouriteImageView = itemView.findViewById(R.id.ac_imv_favourite);
        }

    }
}
