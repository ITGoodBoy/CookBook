package com.celestialapps.cookbook;

import android.content.Context;
import android.content.res.AssetManager;
import android.support.annotation.NonNull;

import com.celestialapps.cookbook.model.Ingredient;
import com.celestialapps.cookbook.model.Recipe;
import com.celestialapps.cookbook.model.RecipeCategory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import io.realm.RealmList;

/**
 * Created by ghost on 17.09.17.
 */

public class CookExtractor {

    private final String INGREDIENT_FOLDER_NAME = "ингредиенты";
    private final String COOK_FOLDER_NAME = "готовка";
    private final String INGREDIENTS_FILE_NAME = "Ингредиенты.txt";
    private final String COOKING_ALGORITHM_FILE_NAME = "Приготовление.txt";

    private final AssetManager mAssetManager;
    private final List<Ingredient> mAllIngredients;
    private final List<String> mAllRecipeFolderPaths;

    public CookExtractor(final Context context) {
        mAssetManager = context.getAssets();
        mAllIngredients = getAllIngredients();
        mAllRecipeFolderPaths = getAllRecipeFolderPaths();
    }


    public List<RecipeCategory> getAllRecipeCategories() {
        List<RecipeCategory> recipeCategories = getAllRecipeCategoryNames();

        for (String recipeFolderPath : getAllRecipeFolderPaths()) {
            Recipe recipe = new Recipe();

            recipe.setName(getRecipeName(recipeFolderPath));
            recipe.setIngredients(getRecipeIngredients(recipeFolderPath));
            recipe.setCookingAlgorithm(getRecipeCookingAlgorithm(recipeFolderPath));
            recipe.setPathToImageFromAssets(getPathToRecipeImageFromAssets(recipeFolderPath));

            for (RecipeCategory recipeCategory : recipeCategories) {
                if (recipeCategory.getName().equals(getRecipeCategoryName(recipeFolderPath))) {
                    recipeCategory.getRecipes().add(recipe);
                }
            }

        }

        return recipeCategories;
    }

    private List<RecipeCategory> getAllRecipeCategoryNames() {
        List<RecipeCategory> recipeCategories = new ArrayList<>();
        try {
            for (String name : mAssetManager.list(COOK_FOLDER_NAME)) {
                RecipeCategory recipeCategory = new RecipeCategory();

                recipeCategory.setName(name);
                recipeCategory.setRecipes(new RealmList<Recipe>());
                recipeCategory.setPathToImageFromAssets(getPathToCategoryImageFromAssets(name));

                recipeCategories.add(recipeCategory);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return recipeCategories;
    }

    @NonNull
    private String getPathToCategoryImageFromAssets(String name) {
        return COOK_FOLDER_NAME + "/" + name + "/" + name + ".jpg";
    }

    private String getPathToRecipeImageFromAssets(String folderPath) {
        return folderPath + getRecipeName(folderPath) + ".jpg";
    }

    private String getRecipeCategoryName(String folderPath) {
        String[] folders = folderPath.split("/");

        return folders[1];
    }

    private String getRecipeCookingAlgorithm(String folderPath) {
        StringBuilder cookingAlgorithmBuilder = new StringBuilder();
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(mAssetManager.open(folderPath + COOKING_ALGORITHM_FILE_NAME)));
            while (bufferedReader.ready()) {
                cookingAlgorithmBuilder.append(bufferedReader.readLine());
                cookingAlgorithmBuilder.append("\n");
            }
        } catch (IOException e) {

        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return cookingAlgorithmBuilder.toString();
    }

    private String getRecipeName(String folderPath) {
        String[] folders = folderPath.split("/");

        return folders[folders.length - 1];
    }

    private RealmList<Ingredient> getRecipeIngredients(String folderPath) {
        RealmList<Ingredient> recipeIngredients = new RealmList<>();
        BufferedReader bufferedReader = null;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(mAssetManager.open(folderPath + INGREDIENTS_FILE_NAME)));
            while (bufferedReader.ready()) {
                String ingredientLine = bufferedReader.readLine();
                String[] ingredientAndCount = ingredientLine.split(" - ");

                String ingredient = ingredientAndCount[0];
                String count = ingredientAndCount[1];

                recipeIngredients.add(new Ingredient(ingredient, count, ""));
            }

            for (Ingredient ingredient : getAllIngredients()) {
                for (Ingredient recipeIngredient : recipeIngredients) {
                    if (recipeIngredient.getName().equals(ingredient.getName()))
                        recipeIngredient.setPathToImageFromAssets(ingredient.getPathToImageFromAssets());
                }
            }

        } catch (IOException e) {

        } finally {
            try {
                if (bufferedReader != null) {
                    bufferedReader.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return recipeIngredients;
    }

    public List<Ingredient> getAllIngredients() {
        if (mAllIngredients != null) return mAllIngredients;

        List<Ingredient> ingredients = new ArrayList<>();

        for (String ingredientFolderPath : getAllIngredientFolderPaths()) {
            String ingredientFolderName = getIngredientFolderName(ingredientFolderPath);
            String pathToIngredientImageFromAssets = getPathToIngredientImageFromAssets(ingredientFolderPath);

            Ingredient ingredient = new Ingredient();
            ingredient.setName(ingredientFolderName);
            ingredient.setPathToImageFromAssets(pathToIngredientImageFromAssets);

            ingredients.add(ingredient);
        }

        return ingredients;
    }

    private String getPathToIngredientImageFromAssets(String ingredientFolderPath) {
        return ingredientFolderPath + getIngredientFolderName(ingredientFolderPath) + ".jpg";
    }

    private String getIngredientFolderName(String ingredientFolderPath) {
        return ingredientFolderPath.split("/")[1];
    }

    private List<String> getAllIngredientFolderPaths() {
        List<String> ingredientFolderPaths = new ArrayList<>();

        try {
            for (String ingredientFolderName : mAssetManager.list(INGREDIENT_FOLDER_NAME)) {
                ingredientFolderPaths.add(INGREDIENT_FOLDER_NAME + "/" + ingredientFolderName + "/");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return ingredientFolderPaths;
    }

    private List<String> getAllRecipeFolderPaths() {
        if (mAllRecipeFolderPaths != null) return mAllRecipeFolderPaths;

        List<String> recipeFolderPaths = new ArrayList<>();

        try {
            for (String categoryName : mAssetManager.list(COOK_FOLDER_NAME)) {
                for (String recipeFolderName : mAssetManager.list(COOK_FOLDER_NAME + "/" + categoryName)) {
                    if (recipeFolderName.contains(".jpg")) continue;
                    recipeFolderPaths.add(COOK_FOLDER_NAME + "/" + categoryName + "/" + recipeFolderName + "/");
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        return recipeFolderPaths;
    }


}
