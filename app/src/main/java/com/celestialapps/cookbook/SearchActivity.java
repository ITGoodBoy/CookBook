package com.celestialapps.cookbook;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.celestialapps.cookbook.model.Ingredient;
import com.celestialapps.cookbook.model.Recipe;

import java.util.ArrayList;
import java.util.List;

import info.hoang8f.widget.FButton;
import io.realm.Realm;
import io.realm.Sort;


/**
 * Created by ghost on 14.10.17.
 */

public class SearchActivity extends AppCompatActivity {

    private FButton fButton;
    private RecyclerView ingredientsRecyclerView;
    private RecyclerView recipesRecyclerView;
    private AutoCompleteTextView autoCompleteTextView;
    private AutoCompleteTextView autoCompleteTextView2;
    private CheckBox onlySelectedIngredientsButton;
    private List<Ingredient> allIngredients;
    public  List<Recipe> SEARCH_RECIPES;

    private void setupActionBar() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) actionBar.setTitle("Поиск по ингредиентам");
    }

    public void setSearchRecipes(List<Recipe> recipes) {
        this.SEARCH_RECIPES = recipes;
    }


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        setupActionBar();

        allIngredients = Realm.getDefaultInstance().where(Ingredient.class).findAllSorted("name", Sort.ASCENDING);
        initViews();
    }


    private void initViews() {
        onlySelectedIngredientsButton = (CheckBox) findViewById(R.id.only_selected_ingredients_button);
        SEARCH_RECIPES = SplashActivity.getRecipeListByIngredientsChecked();

        autoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.searchAutoComplete);
        autoCompleteTextView2 = (AutoCompleteTextView) findViewById(R.id.searchAutoComplete2);

        final SingleRecipesAdapter singleRecipesAdapter = new SingleRecipesAdapter(this, SEARCH_RECIPES);
        final IngredientsAdapter ingredientsAdapter = new IngredientsAdapter(this, true, this, allIngredients, singleRecipesAdapter, autoCompleteTextView2);

        autoCompleteTextView.getBackground().setColorFilter(Color.parseColor("#E8820C"), PorterDuff.Mode.SRC_IN);
        final ArrayAdapter<String> allIngredientsArrayAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, getAllIngredientNames());
        autoCompleteTextView.setAdapter(allIngredientsArrayAdapter);
        autoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                ingredientsAdapter.filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        autoCompleteTextView2.getBackground().setColorFilter(Color.parseColor("#E8820C"), PorterDuff.Mode.SRC_IN);
        autoCompleteTextView2.setAdapter(new ArrayAdapter<>(this,
                android.R.layout.simple_dropdown_item_1line,
                getRecipeNames()));
        autoCompleteTextView2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                singleRecipesAdapter.filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        fButton = (FButton) findViewById(R.id.f_button);
        fButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Realm.getDefaultInstance().executeTransaction(new Realm.Transaction() {
                    @Override
                    public void execute(Realm realm) {
                        for (Recipe recipe : SplashActivity.getRecipeListByIngredientsChecked()) {
                            recipe.setCheckedCount(0);
                        }

                        for (Ingredient ingredient : SplashActivity.getCheckedIngredients()) {
                            ingredient.setState(0);
                        }
                        ingredientsAdapter.refreshSingleRecipesAdapter();
                        singleRecipesAdapter.setDataAndRefresh(SEARCH_RECIPES);
                        autoCompleteTextView.setText("");
                        autoCompleteTextView.setAdapter(allIngredientsArrayAdapter);
                        autoCompleteTextView2.setText("");
                        autoCompleteTextView2.setAdapter(null);
                        ingredientsRecyclerView.setAdapter(ingredientsAdapter);
                        onlySelectedIngredientsButton.setChecked(false);
                    }
                });
            }
        });

        recipesRecyclerView = (RecyclerView) findViewById(R.id.recipes_search);
        recipesRecyclerView.setItemViewCacheSize(50);
        recipesRecyclerView.setDrawingCacheEnabled(true);
        recipesRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_AUTO);
        recipesRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        recipesRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));
        recipesRecyclerView.setAdapter(singleRecipesAdapter);

        ingredientsRecyclerView = (RecyclerView) findViewById(R.id.ingredients_search);
        ingredientsRecyclerView.setItemViewCacheSize(50);
        ingredientsRecyclerView.setDrawingCacheEnabled(true);
        ingredientsRecyclerView.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_AUTO);
        ingredientsRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        ingredientsRecyclerView.addItemDecoration(new DividerItemDecoration(getApplicationContext(), DividerItemDecoration.VERTICAL));
        ingredientsRecyclerView.setAdapter(ingredientsAdapter);


        onlySelectedIngredientsButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                saveOnlySelectedIngredientsState(b);
                if (b) {
                    IngredientsAdapter ingredientsAdapter2 =
                            new IngredientsAdapter(
                                    SearchActivity.this,
                                    true,
                                    SearchActivity.this,
                                    SplashActivity.getCheckedIngredients(),
                                    singleRecipesAdapter,
                                    autoCompleteTextView2);

                    autoCompleteTextView.setAdapter(new ArrayAdapter<>(SearchActivity.this, android.R.layout.simple_dropdown_item_1line, getAllCheckedIngredientNames()));
                    autoCompleteTextView.addTextChangedListener(getIngredientsTextWatcher(ingredientsAdapter2));
                    ingredientsRecyclerView.setAdapter(ingredientsAdapter2);
                }
                else {
                    autoCompleteTextView.setAdapter(allIngredientsArrayAdapter);
                    autoCompleteTextView.addTextChangedListener(getIngredientsTextWatcher(ingredientsAdapter));
                    ingredientsRecyclerView.setAdapter(ingredientsAdapter);
                }
            }
        });

        onlySelectedIngredientsButton.setChecked(PreferenceManager.getDefaultSharedPreferences(this).getBoolean("onlySelectedIngredientsButton", false));
        onlySelectedIngredientsButton.callOnClick();
    }

    private void saveOnlySelectedIngredientsState(boolean b) {
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(SearchActivity.this);
        SharedPreferences.Editor editor =  sharedPreferences.edit();
        editor.putBoolean("onlySelectedIngredientsButton", b);
        editor.apply();
    }

    private TextWatcher getIngredientsTextWatcher(final IngredientsAdapter ingredientsAdapter) {
      return  new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                ingredientsAdapter.filter(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };
    }

    private List<String> getRecipeNames() {
        List<String> list = new ArrayList<>();

        for (Recipe recipe : SEARCH_RECIPES) {
            list.add(recipe.getName());
        }

        return list;
    }


    private List<String> getAllIngredientNames() {
        List<String> ingredientNames = new ArrayList<>();

        for (Ingredient ingredient : allIngredients) {
            ingredientNames.add(ingredient.getName());
        }

        return ingredientNames;
    }

    private List<String> getAllCheckedIngredientNames() {
        List<String> ingredientNames = new ArrayList<>();

        for (Ingredient ingredient : SplashActivity.getCheckedIngredients()) {
            ingredientNames.add(ingredient.getName());
        }

        return ingredientNames;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.question_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.question) {
            startActivity(new Intent(getApplicationContext(), QuestionActivity.class));
        }

        return super.onOptionsItemSelected(item);
    }


}
