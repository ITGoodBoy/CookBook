package com.celestialapps.cookbook;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;

import com.celestialapps.cookbook.model.Ingredient;
import com.celestialapps.cookbook.model.Recipe;
import com.google.android.gms.ads.MobileAds;

import java.util.ArrayList;
import java.util.List;


import io.realm.DynamicRealm;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmMigration;
import io.realm.RealmObjectSchema;
import io.realm.RealmQuery;
import io.realm.RealmSchema;
import io.realm.Sort;

/**
 * Created by ghost on 19.09.17.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        Log.e("SplashActivity", "onCreate");
        super.onCreate(savedInstanceState);
        setToFullScreen();
        setContentView(R.layout.activity_splash);
        hideActionBar();
        MobileAds.initialize(this, "ca-app-pub-5067148654854402~6496998589");

        if (!isHasReadPermission()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 1997);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == 1997 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            initData();
        } else finish();
    }

    private boolean isHasReadPermission() {
        return ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED;
    }


    @Override
    protected void onResume() {
        Log.e("SplashActivity", "onResume");
        if (isHasReadPermission())
            initData();
        super.onResume();
    }

    private void initData() {
        Log.e("SplashActivity", "initData");
        initRealm();
        getDataFromRealm();
    }


    private void initRealm() {
        Log.e("SplashActivity", "initRealm");

        Realm.init(getApplicationContext());

        RealmConfiguration realmConfiguration = new RealmConfiguration.Builder()
                .schemaVersion(1)
                .assetFile("default.realm")
                .migration(new Migration())
                .build();

        Realm.setDefaultConfiguration(realmConfiguration);
    }



    private void getDataFromRealm() {
        Log.e("SplashActivity", "getDataFromRealm");

        startActivity(new Intent(getApplicationContext(), MainActivity.class));
        finish();
    }


    private void setToFullScreen() {
        Log.e("SplashActivity", "setToFullScreen");

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
    }

    public static List<Ingredient> getCheckedIngredients() {
        return Realm.getDefaultInstance().where(Ingredient.class).equalTo("state", 1).or().equalTo("state", 2)
                .findAllSorted("name");
    }

    public static List<Recipe> getRecipeListByIngredientsChecked() {

        RealmQuery<Recipe> realmQuery = Realm
                .getDefaultInstance()
                .where(Recipe.class)
                .equalTo("ingredients.state", 1)
                .not()
                .equalTo("ingredients.state", 2);

        List<Recipe> recipes = realmQuery.findAllSorted("checkedCount", Sort.DESCENDING);

        List<Recipe> recipeList = new ArrayList<>();
        recipeList.addAll(recipes);

        return recipeList;
    }


    private void hideActionBar() {
        Log.e("SplashActivity", "hideActionBar");

        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null)
            actionBar.hide();
    }
}
